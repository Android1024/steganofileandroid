package ru.android1024.staganofile;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by An on 02.06.2015.
 */
public class StegoModel {
    private Bitmap bitmap;
    private File file;
    private String text;
    private String password;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
