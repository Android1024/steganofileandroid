package ru.android1024.staganofile.steganography;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;

import ru.android1024.staganofile.StegoModel;

/**
 * Created by An on 07.04.2015.
 */
public class Steganography {
    private static final String LOG = "Steganography";
    private Bitmap bitmap;
    private static final int T = 25;
    private int index;
    private int lengthMessage;
    private StringBuilder strBuildMessage;


    public void embedding(StegoModel stego) {
        setBitmap(stego.getBitmap());

        Log.w(LOG, "Embedding Stego File");
        StringBuilder textBit = splitBit(stego.getText(),11);
        System.out.println("Binary code  message of text =" + textBit);

        TreeSet<Integer> setPassWay = makesWay(stego.getPassword());
        TreeSet<Integer> setGenForX = generationForX();
        this.bitmap = stego.getBitmap().copy(stego.getBitmap().getConfig(), true);
        try {
            int countBlock = 1;
            embeddingMetaData(setPassWay, setGenForX, textBit, stego.getText().length());
             while (embeddingInBlock(setPassWay, setGenForX, textBit, countBlock) == false) ;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean embeddingInBlock(TreeSet<Integer> setY,
                                     TreeSet<Integer> setX,
                                     StringBuilder textBit,
                                     int countBlock) throws IOException {

        int index = getIndex();
        for (Integer i : setY)
            for (Integer x : setX) {
                int y = countBlock * T + i;
                index++;
                if (index >= textBit.length()) {
                    setIndex(0);
                    save();
                    return true;
                }

                Log.w(LOG,  " X = " + x + " Y = " + y + " index = " + index + " textBit.length()=" + textBit.length());
                int byteText = (int) textBit.charAt(index);
                replace(x, y, byteText);
            }
        setIndex(index);
        return false;
    }

    public void recovery(StegoModel stego) {
        setBitmap(stego.getBitmap());
        strBuildMessage = new StringBuilder();

        TreeSet<Integer> setPassWay = makesWay(stego.getPassword());
        TreeSet<Integer> setGenForX = generationForX();

        Log.w(LOG, "X size = " + setGenForX.size());
        try {
             int countBlock=1;
             recoveryMetaData(setPassWay, setGenForX);
             while(recoveryInBlock(setPassWay,setGenForX,countBlock)==false);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    private boolean embeddingMetaData(TreeSet<Integer> setY,
                                      TreeSet<Integer> setX,
                                      StringBuilder textBit,
                                      int length) throws IOException {
        Log.w(LOG, "embedding metadate");
        int index = getIndex();

        int indexLength = 0;
        StringBuilder binaryLengthMessage = splitBit(String.valueOf(length),8);
        Iterator<Integer> it = setY.iterator();
        int y = it.next();
        for (Integer x : setX) {
          //  Log.w(LOG, " x=" + x + " y=" + y + " 1");
            replace(x, y, (byte) '1'); // Встраиваем метку что есть такое.
        }
        y = it.next();
        for (Integer x : setX) {
            if (indexLength < binaryLengthMessage.length()) {
                replace(x, y, (byte) binaryLengthMessage.charAt(indexLength++));//Встраиваем длину сообщения.
             //   Log.w(LOG, " x=" + x + " y=" + y + " 0");
            } else replace(x, y, (byte) '0');
        }

        while(it.hasNext()) {
           y = it.next();
            for (Integer x : setX) {
                if (textBit.length() > index)
                    replace(x, y, (int) textBit.charAt(index++));
            }
        }
        setIndex(index);
        Log.w(LOG, "embedding end metadate");
    return false;
}

    private boolean recoveryMetaData(TreeSet<Integer> setY,
                                     TreeSet<Integer> setX) {

        Iterator<Integer> it = setY.iterator();
        int y = it.next();
        //Проверка на наличие существование стего сообщения!
        for (Integer x : setX) {
            char c = getCharLastBit(x, y);
            // Log.w(LOG,"x="+ x + " y="+y+" value= " + getCharLastBit(x, y));
            if (c == '0') return false;
        }
        // Достаем длину сообщения
        y = it.next();
        StringBuilder sBuild = new StringBuilder();
        for (Integer x : setX) {
            Log.w(LOG, "x=" + x + " y=" + y + " value= " + getCharLastBit(x, y));
            sBuild.append(getCharLastBit(x, y));
        }

        Log.w(LOG, "Build=" + sBuild.toString());
        Log.w(LOG, "MakeBit=" + makeBitLengthMessage(sBuild.toString()));
        setLengthMessage(11*Integer.valueOf(makeBitLengthMessage(sBuild.toString())));
        Log.w(LOG, "" + getLengthMessage());

        while(it.hasNext()) {
            y = it.next();
            for (Integer x : setX) {
                int pix = getBitmap().getPixel(x, y);
                String s = Integer.toBinaryString(pix);
                getStrBuildMessage().append(s.charAt(s.length() - 1));
                if ( getStrBuildMessage().length()>getLengthMessage()) {
                    Log.w(LOG, getStrBuildMessage().toString());
                    return true;
                }
            }
        }
        return true;

    }
    private boolean recoveryInBlock(TreeSet<Integer> setY,
                                    TreeSet<Integer> setX, int countBlock) {
        int k = 0;
        for (Integer i : setY) {
            int y  = countBlock * T + i;
            for (Integer j : setX) {
                int pix = getBitmap().getPixel(j, y);
                String s = Integer.toBinaryString(pix);
                getStrBuildMessage().append(s.charAt(s.length() - 1));
                k++;
                Log.w(LOG,"length = "+ getStrBuildMessage().length());
                if ( getStrBuildMessage().length()>getLengthMessage()) {
                    Log.w(LOG, getStrBuildMessage().toString());
                    Log.w(LOG, makeBit(getStrBuildMessage().toString()));
                    return true;
                }
            }
        }
        return false;
    }

    private char getCharLastBit(int x, Integer y) {
        int pix = getBitmap().getPixel(x, y);
        String s = Integer.toBinaryString(pix);
        return s.charAt(s.length() - 1);
    }

    public String makeBitLengthMessage(String bits) {
        StringBuilder collect = new StringBuilder();
        int k = 0;

        while (true) {
            StringBuilder str = new StringBuilder();
            if (k + 8 > bits.length()) break;
            for (int i = 0; i < 8; i++) {
                str.append(bits.charAt(i + k));
            }

            if (str.toString().compareTo("00000000") == 0) break;
            k += 8;
            int x = Integer.parseInt(str.toString(), 2);
            collect.append((char) x);
        }
        return collect.toString();
    }

    public String makeBit(String bits) {
        StringBuilder makeRecoveryString = new StringBuilder();
        for (int i = 0; i < bits.length() /11; i++) {
            StringBuilder strChar = new StringBuilder();
            for (int k = 0; k < 11; k++) {
                strChar.append(bits.charAt(i*11 + k));
            }
            Log.w(LOG,"str= "+strChar.toString());
            int x = Integer.parseInt(strChar.toString(), 2);
            Log.w(LOG, "int x = "+x);
            makeRecoveryString.append((char) x);
        }
        return makeRecoveryString.toString();
    }



    private void replace(Integer x, int y, int value) throws IOException {
        Replace per = new Replace();
        int p = per.lastBlueBit(bitmap, value, x, y);
        // Log.e(LOG,"old Pixel="+bitmap.getPixel(x,y));
        bitmap.setPixel(x, y, p);
        //  Log.e(LOG,"new Pixel="+bitmap.getPixel(x,y));
    }

    private StringBuilder splitBit(String text, int kol) {
        StringBuilder bits = new StringBuilder();
        System.out.println("text= " + text);
        for (int i = 0; i < text.length(); i++) {
            String binaryValue = Integer.toBinaryString((int) text.charAt(i));
            while (binaryValue.length() < kol) binaryValue = "0" + binaryValue;
            Log.w(LOG, "binaryV=" + binaryValue + " int = " + (int) text.charAt(i) +" byte="+(byte)text.charAt(i)+ " str[i]=" + text.charAt(i));
            bits.append(binaryValue);
        }
        System.out.println("text on bits= " + bits);
        return bits;
    }

    private void save() {
        Bitmap finalBitmap = getBitmap();
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/SteganoFile");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        Log.e(LOG, "Save");
        String fname = "Image-" + n + ".png";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            Log.e(LOG, "outstream bitmap");
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    /**
     * @param password
     */
    private static TreeSet<Integer> makesWay(String password) {
        MD5 md5 = new MD5();
        String hashPass = md5.getHash(password);
        return convertASCII(hashPass);
    }

    private static TreeSet<Integer> convertASCII(String hashPass) {
        TreeSet<Integer> set = new TreeSet<Integer>();
        System.out.println(hashPass);
        for (int i = 0; i < hashPass.length(); i++) {
            set.add((int) hashPass.charAt(i));
        }
        return set;
    }

    private TreeSet<Integer> generationForX() {
        TreeSet<Integer> set = new TreeSet<Integer>();
        for (int i = 0; i < T; i++) {
            set.add(getBitmap().getWidth() * i / T);
        }
        return set;
    }

    private void setLengthMessage(int lengthMessage) {
        this.lengthMessage = lengthMessage;
    }

    private int getLengthMessage() {
        return this.lengthMessage;
    }


    private void setIndex(int index) {
        this.index = index;
    }

    private int getIndex() {
        return this.index;
    }

    public StringBuilder getStrBuildMessage() {
        return strBuildMessage;
    }

    public void setStrBuildMessage(StringBuilder strBuildMessage) {
        this.strBuildMessage = strBuildMessage;
    }

}
