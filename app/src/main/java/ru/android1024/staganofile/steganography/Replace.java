package ru.android1024.staganofile.steganography;

import android.graphics.Bitmap;

import java.io.IOException;
import java.util.ArrayList;

public class Replace {
    public int lastBlueBit(Bitmap image, int lastBitsReplace, int x,
                                 int y) throws IOException {
        int bitNew = image.getPixel(x, y);
        bitNew = bitNew & 0b11111111111111111111111111111110;
        bitNew = bitNew | (lastBitsReplace & 0b111);
        return bitNew;
    }
    public int lastTwoBlueBit(Bitmap image, byte lastBitsReplace, int x,
                            int y) throws IOException {
        int bitNew = image.getPixel(x, y);
        bitNew = bitNew & 0b11111111111111111111111111111100;
        bitNew = bitNew | (lastBitsReplace & 0b111);
        return bitNew;
    }

    public void rgb(Bitmap image, int x, int y) {
        int rgb = image.getPixel(x, y);
        int o = (rgb & 0xFF000000) >>> 24;
        int r = (rgb & 0x00FF0000) >>> 16;
        int g = (rgb & 0x0000FF00) >>> 8;
        int b = (rgb & 0x000000FF) >>> 0;

        if (o != 0xFF) {
            System.out.println("���! " + o);
        }

        if (r != g && g != b) {
            System.out.println(" grayscale. " + r + " " + g + " " + b);
        }
        image.setPixel(x, y, o << 24 | r << 16 | g << 8 | b);
    }

    public ArrayList<Byte> byteListImg(Bitmap img) {
        int height = img.getHeight();
        int width = img.getWidth();
        ArrayList<Byte> listByte = new ArrayList<Byte>();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                byte b = (byte) img.getPixel(j, i);
                b = (byte) (b & 0b11);
                listByte.add(b);
            }
        }
        return listByte;
    }
}
