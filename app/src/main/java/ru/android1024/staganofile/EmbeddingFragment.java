package ru.android1024.staganofile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import ru.android1024.staganofile.steganography.Steganography;

public class EmbeddingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    /** Standard activity result: operation canceled. */
    public static final int RESULT_CANCELED    = 0;
    /** Standard activity result: operation succeeded. */
    public static final int RESULT_OK           = -1;
    /** Start of user-defined activity results. */
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    private final String LOG = this.getClass().toString();
    private TextView textAboutImage;
    private String mParam1;
    private EditText textMessage;
    private ImageView imgView;
    private TextView textViewSave;
    private TextView textViewFile;
    private StegoModel stego;
    private Uri mImageCaptureUri;

    public static EmbeddingFragment newInstance(String param1) {
        EmbeddingFragment fragment = new EmbeddingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public EmbeddingFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_embedding, container, false);
        textAboutImage = (TextView) rootView.findViewById(R.id.textViewAboutFileImage);
        textViewSave = (TextView) rootView.findViewById(R.id.textViewSave);
        imgView = (ImageView) rootView.findViewById(R.id.imageView);
        textViewFile = (TextView) rootView.findViewById(R.id.textViewAboutFile);

        stego = new StegoModel();

        Button btnChooseImage = (Button) rootView.findViewById(R.id.buttonChoiseImage);
        btnChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialogShow();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Button btnChooseFile = (Button) rootView.findViewById(R.id.buttonChoiseDoc);
        btnChooseFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                         intent.setType("image/*");
//                          startActivityForResult(intent, 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        textMessage = (EditText) rootView.findViewById(R.id.editTextEmbbeding);
        Button btnSave = (Button) rootView.findViewById(R.id.buttonSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showDialogPassword();
                showMessageDialog();
            }
        });
        return rootView;
    }
    private void dialogShow() {
        final String[] items = new String[]{"Из Фотографии", "Из SD Card"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, items);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());

        builder.setTitle("Меню");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(),
                            "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                    mImageCaptureUri = Uri.fromFile(file);

                    try {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                        intent.putExtra("return-data", true);

                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dialog.cancel();
                } else {
                    Intent intent = new Intent();

                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);

                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
                }
            }
        });
        final android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    protected void showMessageDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        final EditText editTextPassword = new EditText(getActivity());
        alertDialog.setTitle("Пароль");
        alertDialog.setView(editTextPassword).create();
        alertDialog.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                stego.setPassword(editTextPassword.getText().toString());
                stego.setText(textMessage.getText().toString());
                if ( allCorrect(stego)) {
                    embedding();
                    textViewSave.setText("Сохранено успешно");
                }
                else
                    textViewSave.setText("Ошибка");
            }
        });
        alertDialog.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        Toast.makeText(getActivity().getBaseContext(), "Успешно!!", Toast.LENGTH_LONG);
        alertDialog.show();
    }

    private void embedding() {
        Steganography steganography = new Steganography();
        steganography.embedding(stego);
    }

    private boolean allCorrect(StegoModel stego) {
        if ( stego.getText() == null ) return false;
        if ( stego.getPassword()==null) return false;
        if ( stego.getBitmap()==null) return false;
        return true;
    }


    void readFileSD(String filePath) {
        // проверяем доступность SD
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.w(LOG, "SD-карта не доступна: " + Environment.getExternalStorageState());
            return;
        }
        Log.w(LOG, "SD-карта: ");
        InputStream is = null;
        try {
            // получаем путь к SD
            //File sdPath = Environment.getExternalStorageDirectory();
            File sdFile = new File(filePath);
            Log.w(LOG, sdFile.getPath() + " = " + filePath);
            is = new BufferedInputStream(new FileInputStream(sdFile));
            byte[] byteFile = new byte[(int) sdFile.length()];
            while ((is.read(byteFile, 0, (int) sdFile.length())) != -1) {
                // Log.w(LOG, val+"."+String.valueOf((int)val)+ " "+(int)val);
            }
         //   setBytesMessage(byteFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        Bitmap bitmap = null;
        String path = "";

        if (requestCode == PICK_FROM_FILE) {
            mImageCaptureUri = data.getData();
            path = getRealPathFromURI(mImageCaptureUri); //from Gallery

            if (path == null)
                path = mImageCaptureUri.getPath(); //from File Manager

            if (path != null)
                bitmap = BitmapFactory.decodeFile(path);
        } else {
            path = mImageCaptureUri.getPath();
            bitmap = BitmapFactory.decodeFile(path);
        }
        if ( bitmap == null ) textAboutImage.setText("Null image");
         else {
            stego.setBitmap(bitmap);
          textAboutImage.setText("Файл:"+path+"\nРазмеры:" +bitmap.getWidth()+ "*"+bitmap.getHeight());
            imgView.setImageBitmap(bitmap);
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
