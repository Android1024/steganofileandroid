package ru.android1024.staganofile;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import ru.android1024.staganofile.steganography.Steganography;

public class RecoveryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String LOG = "Recovery";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TextView textRecovery;
    private EditText password;
    private ImageView stegoImage;
    private static final int PICK_FROM_FILE = 2;
    private Uri mImageCaptureUri;
    private StegoModel stego;

    public static RecoveryFragment newInstance(String param1, String param2) {
        RecoveryFragment fragment = new RecoveryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_recovery, container, false);
        textRecovery = (TextView) rootView.findViewById(R.id.textViewStegoFile);
        stegoImage = (ImageView) rootView.findViewById(R.id.imageViewStego);
        password = (EditText) rootView.findViewById(R.id.editTextPassword);
        stego = new StegoModel();

        Button buttonPick = (Button) rootView.findViewById(R.id.btnChoiseSaveFile);
        buttonPick.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                try {
                    dialogShow();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        Button buttonRecovery = (Button) rootView.findViewById(R.id.buttonRecovery);
        buttonRecovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( stego.getBitmap() == null) {
                    Toast.makeText(getActivity(),"No image",Toast.LENGTH_LONG).show();
                } else {
                    stego.setPassword(password.getText().toString());
                    new Steganography().recovery(stego);
                }
            }
        });
        return rootView;
    }
    private void dialogShow() {

        Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != getActivity().RESULT_OK) return;

        Bitmap bitmap = null;
        String path = "";

        if (requestCode == PICK_FROM_FILE) {
            mImageCaptureUri = data.getData();
            path = getRealPathFromURI(mImageCaptureUri); //from Gallery

            if (path == null)
                path = mImageCaptureUri.getPath(); //from File Manager

            if (path != null)
                bitmap = BitmapFactory.decodeFile(path);
        } else {
            path = mImageCaptureUri.getPath();
            bitmap = BitmapFactory.decodeFile(path);
        }
        stego.setBitmap(bitmap);
        stegoImage.setImageBitmap(bitmap);
    }
    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);

        if (cursor == null) return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        return cursor.getString(column_index);
    }



}
